---
layout: handbook-page-toc
title: LifeLabs Learning
description: "Manager Enablement Training with LifeLabs Learning"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

As part of GiLab's focus on equipping people leaders with the skills and tools to lead all-remote teams, L&D has partnered with [LifeLabs Learning](https://lifelabslearning.com/) to deliver management training. The program is intended to provide additional training for managers as part of their management journey and customized GitLab management programs (i.e. [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/)) 

## Goal

GitLab requires exceptional leaders and powerful teams. The goal of LifeLabs Learning is to give managers, execs, and people leaders an opportunity to develop core leadership skills that make the biggest impact in the shortest time. As a participant in LifeLabs Learning, you will gain the essential building blocks of a high-performing manager, leading all-remote teams.

As a participant in LifeLabs learning, you will gain the essential building blocks of a high-performing manager, leading all-remote teams. 

### What it is not

Participating in LifeLabs Learning is not a ticket to a promotion or raise. Instead, it is a growth & development opportunity that will lead to skill development to help improve your leadership skills. 

## Benefits for the company

Apart from creating leadership development opportunities, participating in LifeLabs Learning will: 

1. Improve employee engagement
2. Increase growth & development opportunities for managers and up
3. Improve communication by managers to direct reports on providing feedback, solving problems, motivating team members, performance management, and resolving conflicts

## Program Details

LifeLabs Learning offers two core manager development programs. Each workshop is 2 hours, with a 10 participant max. 

### [Manager Core 1 Curriculum](https://drive.google.com/file/d/1MJmxjrMSSCq3lWOOks-vMnzdzPucI0jp/view)

One Session every 2 weeks: 
1. Coaching Skills: Learn how to use questions to engage, empower, and improve performance in your direct reports.
2. Feedback Skills: Give performance changing feedback even when the topic is tough and time is limited.
3. Productivity & Prioritization: Collect tools to help you and your team make progress on the most important initatives.
4. Effective 1-1's: Use 1:1's to maximize people's feelings of certainty, progress, inclusion, growth, and engagement

3 months later: 
5. Manager Intensive 1: Review what you’ve learned so far and apply these skills to tough, real-life management scenarios. 

### Manager Core 2 Curriculum

The [Manager Core 2 program](https://drive.google.com/file/d/1f0HhqBfGn1lnaYSMHcYYqauwyOkNHS0R/view) is recommended to take at least 3 months or later after the Manager Intensive 1 has been completed. 

One session every 2 weeks: 
1. Strategic Thinking: Help people make better decisions even in the face of uncertain variables.
2. Meetings Mastery: Use advanced facilitation tools and techniques to engineer meetings that are useful, productive, and energizing.
3. Leading Change: Equip your teams to be more adaptive, resilient, and agile.
4. People Development: Equip your teams to be more adaptive, resilient, and agile. 

3 months later: 
5. Manager Intensive 2: Review what you’ve learned so far and apply these skills to tough, real-life management scenarios.

## Participate

### Eligibility 

You are eligible to apply for the program if you are a: 
 
1. [Manager or up](https://about.gitlab.com/company/team/structure/#layers), [Product Manager or up](https://about.gitlab.com/job-families/product/product-manager/), Staff Engineer

If you meet these requirements and are interested in participating in LifeLabs Learning Manager Core 1 and Core 2 programs, please add yourself to the waitlist using [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/53). Follow the [#lifelabs-learning Slack channel](https://app.slack.com/client/T02592416/C036Y740SKY) for current announcements.

### Current Programs

From May to September of 2022, a second cohort of 40 manangers will complete the program. Team members can view the most up to date program information in [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/60).

### Past Program Results

1. In FY23Q1-Q2, a pilot of LifeLabs Learning was hosted with 10 cross-functional people leaders across the organization. Key reflections and resources can be found in [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/365)


